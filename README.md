SysML to Modelica Transformation Eclipse Plugin
===============================================

edu.ufc.femtost.disc.sysml2modelica

ATL and Acceleo based transformation from SysML to Modelica for Papyrus Luna.

Implementation of the SysML-Modelica Transformation Specification Version 1.0 November 2012

1. Download and install Eclipse Luna MDT
2. From help->install modeling component : install ATL, Acceleo and Papyrus
3. Get the source code from the Git
4. Install the SysML4Modelica plugin from the update site project edu.ufc.femtost.disc.sysml4modelica.papyrus.updatesite 
5. Import the example edu.ufc.femtost.disc.sysml4modelica.papyrus.example
7. Right click on the Two_tanks.uml file -> SysML4Modelica -> Generate Modelica code	
	
